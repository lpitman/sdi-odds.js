var _ = require('lodash');
var Fraction = require('fraction.js');
var numeral = require('numeral');

function Lib(config, args) {
  this._config = {};
  this.config({num: 'num', den: 'den'});
  this.config(config);
  this._f = new Fraction(0, 1);
  this.parse.apply(this, args);
}

Lib.prototype.config = function (config) {
  if (!config) return this._config;
  _.merge(this._config, config || {});
  return this;
};

Lib.prototype.parse = function (arg1, arg2) {
  if (_.isArray(arg1)) {
    this._f = new Fraction(arg1);
  } else if (_.isObject(arg1)) {
    var num = arg1[this._config.num];
    var den = arg1[this._config.den];
    this._f = new Fraction(num, den);
  } else if (arg2) {
    this._f = new Fraction(arg1, arg2);
  } else if (!_.isUndefined(arg1))  {
    arg1 = '' + arg1;
    if (_.includes(arg1, '.')) {
      this.parseDecimal(arg1);
    } else if (_.includes(arg1), '/') {
      this.parseFractional(arg1);
    } else {
      this.parseMoneyline(arg1);
    }
  }

  return this;
};

Lib.prototype.parseDecimal = function (str) {
  var f = new Fraction(str);
  f.sub(1);
  this.num(f.n);
  this.den(f.d);
};

Lib.prototype.parseFractional = function (str) {
  var parts = str.split('/');
  this.num(parts[0]);
  this.den(parts[1]);
};

Lib.prototype.parseMoneyline = function (str) {
  var n = parseFloat(str);
  if (n > 0) n = n / 100;
  if (n < 0) n = -100 / n;
  this._f = new Fraction(n);
};

Lib.prototype.parseAmerican = Lib.prototype.parseMoneyline;

Lib.prototype.frac = function (frac) {
  if (frac) {
    var n = frac[this._config.num];
    var d = frac[this._config.den];
    this._f = new Fraction(n, d);
    return this;
  }

  frac = {};
  frac[this._config.num] = this._f.n;
  frac[this._config.den] = this._f.d;
  return frac;
};

Lib.prototype.num = function (num) {
  if (!num) return this._f.n;
  this._f = new Fraction(num, this._f.d);
  return this;
};

Lib.prototype.den = function (den) {
  if (!den) return this._f.d;
  this._f = new Fraction(this._f.n, den);
  return this;
};

Lib.prototype.format = function (type) {
  type = type || 'fractional';
  type = type.toLowerCase();
  var fn = 'format' + _.capitalize(type || 'fractional');
  return this[fn].call(this);
};

Lib.prototype.formatDecimal = function () {
  var f = new Fraction(this._f.n, this._f.d);
  f.add(1);
  return numeral(f.valueOf()).format('0.00');
};

Lib.prototype.formatFractional = function () {
  //var num = this._f.n;
  //var denom = this._f.d;
  //
  //while(denom.length > 3) {
  //  denom = denom.substr(0, denom.length-1);
  //
  //}
  return this._f.n + '/' + this._f.d;
};

Lib.prototype.formatMoneyline = function () {
  if (this._f.n === 0) return '0';
  var f = new Fraction(this._f.n, this._f.d);
  f = (f.n >= f.d) ? f.mul(100) : f.reciprocal().mul(-100);
  return numeral(f.valueOf()).format('+0,0');
};

Lib.prototype.formatAmerican = Lib.prototype.formatMoneyline;

module.exports = function () {
  return new Lib(
    {}, Array.prototype.slice.call(arguments)
  );
};

module.exports.global = function (config) {
  return function () {
    return new Lib(
      config, Array.prototype.slice.call(arguments)
    );
  };
};
